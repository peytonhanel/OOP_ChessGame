#ifndef CHESSGAME_CHESSGAME_H
#define CHESSGAME_CHESSGAME_H

#include <vector>
#include "ChessBoard.h"
#include "ChessMovementRule.h"
#include "King.h"

class ChessBoard;


/**
 * Class that represents one instance of a game of chess. Use initialize() to
 * begin the game.
 */
class ChessGame {

public:

   /**
    * Constructs a ChessGame instance.
    *
    * @param useGraphics True will use 3D click-based graphics.
    * @param useAI True will not allow input for player2 and instead the computer
    *              will generate moves.
    */
   ChessGame(bool useGraphics, bool useAI);

   /**
   * Adds a king to the list of kings. For proper use, add all the kings in the
   * game at the start of the game.
   *
   * @param king The king to add.
   *
   * @return True if the king was successfully added. False if king is already
   * listed or if king is not part of this game.
   */
   bool listKing(King* king);

   /**
    * Returns the king for a specified team.
    *
    * @param team The team of the king being returned.
    *
    * @return The king for a specified team.
    */
   King* getKingForTeam(Team* team);

   /**
    * Asks the game if a move is valid.
    *
    * @param piece The piece to move.
    * @param newRank The rank the piece is moving to.
    * @param newFile The file the piece is moving to.
    *
    * @return True if the piece is allowed to move.
    */
   bool isValidMove(Piece* piece, int newRank, int newFile);

   /**
    * Starts the game.
    */
   void initialize();

private:

   /**
    * @Return True if this game has a winner.
    */
   bool hasWinner();

   /**
    * @return The player who's turn it is.
    */
   Team* getCurrentPlayer();

   /**
    * Executes the next turn.
    */
   bool nextTurn(bool allowSurrender);


   bool _usesGraphics;
   bool _usesAI;
   ChessBoard* _board;
   int _currentPlayerIndex;
   Team* _winner;
   std::vector<King*> _kings;

   static MovementRule* _movementRule;
   static Team* _players[2];
   static const std::string& errorMsg()
   {
      static const std::string msg("ERROR: bad input!");
      return msg;
   }

   static const int ASCII_a = 97;
   static const int ASCII_1 = 49;
   static const int SPACE_CHAR = 32;

   static const int START_RANK = 0;
   static const int START_FILE = 1;
   static const int DEST_RANK = 3;
   static const int DEST_FILE = 4;
   static const int SPACE_SPOT = 2;
   static const int DRAW_SPOT = 5;
   static const int INPUT_LENGTH = 5;
   static const int DRAW_INPUT_LENGTH = 6;
};


#endif //CHESSGAME_CHESSGAME_H

#ifndef CHESSGAME_TILE_H
#define CHESSGAME_TILE_H

#include <iostream>
#include <string>
#include "GameBoard.h"

class Piece;
class GameBoard;


/**
 * Class representing a tile on a checkered board.
 */
class Tile {

private:
   int _rank;
   int _file;
   GameBoard* _board;
   Piece* _occupant;

public:

   /**
   * Constructs a tile.
   *
   * @param rank The rank of this tile.
   * @param file The file of this tile.
   * @param board The board this tile belongs to.
   */
   Tile(int rank, int file, GameBoard* board);

   /**
    * @return True if this tile is occupied.
    */
   bool isOccupied();

   /**
    * @return The piece that is on this tile. Null if the tile isn't occupied.
    */
   Piece* getOccupant();

   /**
    * Places a piece on this tile.
    *
    * @param piece The piece to put on this tile.
    */
   void setOccupant(Piece* piece);

   /**
    * @return Gets the rank of this tile.
    */
   int getRank();

   /**
    * @return Gets the file of this tile.
    */
   int getFile();

   /**
    * @return The board this tile is apart of.
    */
   GameBoard* getBoard();

   /**
    * @return The string representation of this tile with it's occupant.
    */
   std::string toString();

   /**
    * Compares two tiles to see if they are the same tile.
    *
    * @param t1 The other tile being compared.
    *
    * @return True if the tiles have the same file and rank and are apart of the
    * same board.
    */
};


#endif //CHESSGAME_TILE_H

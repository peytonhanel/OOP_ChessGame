#ifndef CHESSGAME_QUEEN_H
#define CHESSGAME_QUEEN_H

#include "Team.h"
#include "Tile.h"
#include "Piece.h"
#include "Pawn.h"


/**
 * Class that represents a queen.
 */
class Queen : public Piece {

public:

   /**
    * Constructs a queen.
    *
    * @param team The team this queen belongs to.
    * @param tile The tile this queen starts the game on.
    */
   Queen(Team* team, Tile* tile);

   /**
    * Destructs this Queen.
    */
   ~Queen() override;

   /**
   * Moves this Queen to a new tile.
   *
   * @param rank The rank of the tile being moved to.
   * @param file The file of the tile being moved to.
   *
   * @return True if moved successfully.
   */
   bool moveTo(int rank, int file) override;

   /**
    * @return The symbol representing this queen.
    */
   std::string getSymbol() override;

   /**
    * @return The name of this class.
    */
   static const std::string& type()
   {
      static std::string type("Queen");
      return type;
   }
};


#endif //CHESSGAME_QUEEN_H

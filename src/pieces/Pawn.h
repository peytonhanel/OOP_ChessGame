#ifndef CHESSGAME_PAWN_H
#define CHESSGAME_PAWN_H

#include "Piece.h"


/**
 * class that represents a Pawn.
 */
class Pawn : public Piece {
public:

   /**
    * The direction that this pawn is moving.
    */
   enum MovementDirection {
      Up = 1,
      Down = -1,
   };

  /**
  * Constructs a new pawn.
  *
  * @param team The team this pawn belongs to.
  * @param startingTile The tile this pawn starts the game on.
  */
   Pawn(Team* team, Tile* startingTile, MovementDirection movementDirection);

   /**
    * Destructs this piece.
    */
   ~Pawn() override;

  /**
   * Moves this pawn to a new tile.
   *
   * @param rank The rank of the tile being moved to.
   * @param file The file of the tile being moved to.
   *
   * @return True if moved successfully.
   */
   bool moveTo(int rank, int file) override;

   /**
    * Allow this pawn to move forward two tiles instead of one. If the pawn
    * moves forward one or two steps, then it will not be allowed to move two
    * steps any more, unless this is called again.
    */
   void giveSecondStep();

   /**
    * @return The direction this pawn is allowed to move.
    */
   MovementDirection getDirection();

   /**
    * @return The symbol for this pawn.
    */
   std::string getSymbol() override;

   /**
    * @return The name of this class.
    */
   static const std::string& type()
   {
      static std::string type("Pawn");
      return type;
   }

private:

   bool _canMoveForwardTwo;
   MovementDirection _movementDirection;
};


#endif //CHESSGAME_PAWN_H

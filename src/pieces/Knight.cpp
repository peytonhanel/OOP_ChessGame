#include "Knight.h"

Knight::Knight(Team* team, Tile* startingTile)
   : Piece(team, startingTile, type())
{}

Knight::~Knight() = default;

std::string Knight::getSymbol()
{
   return _team->getShade() == Team::Shade::light ? "wN" : "bN";
}

bool Knight::moveTo(int rank, int file)
{
   bool moved = false;

   int fileSteps = file - _occupyingTile->getFile();
   int rankSteps = rank - _occupyingTile->getRank();

   // move 2 steps in one direction and 1 step in a perpendicular direction
   if (  (abs(fileSteps) == 1 && abs(rankSteps) == 2)
      || (abs(fileSteps) == 2 && abs(rankSteps) == 1))
   {
      moved = Piece::moveTo(rank, file, true, true);
   }

   return moved;
}


#ifndef CHESSGAME_KING_H
#define CHESSGAME_KING_H

#include "Piece.h"


/**
 * A class representing a King in a chess game.
 */
class King : public Piece {

public:
   /**
    * Constructs a new king.
    * @param team The team to belong to.
    * @param startingTile The starting tile of the king.
    */
   King(Team* team, Tile* startingTile);

   /**
    * Destructs this King.
    */
   ~King() override;

   /**
   * Moves this King to a new tile.
   *
   * @param rank The rank of the tile being moved to.
   * @param file The file of the tile being moved to.
   *
   * @return True if moved successfully.
   */
   bool moveTo(int rank, int file) override;

   /**
    * @return True if this king is in check.
    */
   bool isInCheck();

   /**
    * Tells this king if it is in check or not.
    *
    * @param check True if in check, false if not.
    */
   void setCheckState(bool check);

   /**
    * @return A char representing the king.
    */
   std::string getSymbol() override;

   /**
    * @return The name of this class.
    */
   static const std::string& type()
   {
      static std::string type("King");
      return type;
   }

private:

   bool _isInCheck;
};


#endif //CHESSGAME_KING_H

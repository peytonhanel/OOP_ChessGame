#ifndef CHESSGAME_BISHSOP_H
#define CHESSGAME_BISHSOP_H

#include "Team.h"
#include "Tile.h"
#include "Piece.h"

/**
 * A class representing a bishop chess piece.
 */
class Bishop : public Piece {

public:

   /**
    * Constructs a Bishop.
    *
    * @param team The team to belong to.
    * @param startingTile The tile the Bishop starts on.
    */
   Bishop(Team* team, Tile* startingTile);

   /**
    * Destructs this Bishop.
    */
   ~Bishop() override;

   /**
   * Moves this Bishop to a new tile.
   *
   * @param rank The rank of the tile being moved to.
   * @param file The file of the tile being moved to.
   *
   * @return True if moved successfully.
   */
   bool moveTo(int rank, int file) override;

   /**
    * @return The symbol for a bishop.
    */
   std::string getSymbol() override;

   /**
    * @return The name of this class.
    */
   static const std::string& type()
   {
      static std::string type("Bishop");
      return type;
   }
};


#endif //CHESSGAME_BISHSOP_H

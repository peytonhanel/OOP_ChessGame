#include "King.h"

King::King(Team* team, Tile* startingTile)
   : Piece(team, startingTile, type()) {}

King::~King() = default;

std::string King::getSymbol()
{
   return _team->getShade() == Team::Shade::light ? "wK" : "bK";
}

bool King::moveTo(int rank, int file)
{
   bool moved = false;
   int fileSteps = file - _occupyingTile->getFile();
   int rankSteps = rank - _occupyingTile->getRank();

   // move 1 step in any direction
   if (abs(fileSteps) <= 1 && abs(rankSteps) <= 1) {
      moved = Piece::moveTo(rank, file, false, true);
   }
   return moved;
}

void King::setCheckState(bool check)
{
   _isInCheck = check;
}

bool King::isInCheck()
{
   return _isInCheck;
}

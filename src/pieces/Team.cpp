#include "Team.h"

Team* Team::_lightSingleton;
Team* Team::_darkSingleton;


Team::Team(Shade shade)
{
   _shade = shade;
}

Team::Shade Team::getShade()
{
   return _shade;
}

Team* Team::getLightSingleton()
{
   if (_lightSingleton == nullptr) {
      _lightSingleton = new Team(Shade::light);
   }
   return _lightSingleton;
}

Team* Team::getDarkSingleton()
{
   if (_darkSingleton == nullptr) {
      _darkSingleton = new Team(Shade::dark);
   }
   return _darkSingleton;
}

#ifndef CHESSGAME_TEAM_H
#define CHESSGAME_TEAM_H

#include <string>

/**
 * Class that represents a team, or a player, in a board game.
 */
class Team {

public:

   /**
    * The color of this team's pieces.
    */
   enum Shade
   {
      dark,
      light
   };

   /**
    * @return The shade of this team.
    */
   Shade getShade();

   /**
    * @return The singleton for the light team.
    */
   static Team* getLightSingleton();

   /**
    * @return The singleton for the dark team.
    */
   static Team* getDarkSingleton();
   
private:

   /**
    * Constructs a team.
    *
    * @param shade The color of this team's pieces.
    */
   explicit Team(Shade shade);

   static Team* _lightSingleton;
   static Team* _darkSingleton;
   Shade _shade;
};


#endif //CHESSGAME_TEAM_H

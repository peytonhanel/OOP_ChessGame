#ifndef CHESSGAME_CHESSMOVEMENTRULE_H
#define CHESSGAME_CHESSMOVEMENTRULE_H

#include "ChessBoard.h"
#include "Piece.h"
#include "Pawn.h"
#include "King.h"
#include "Rook.h"
#include "Queen.h"
#include "Bishop.h"
#include "Knight.h"


/**
 * A class containing extra rules for piece movement in a game of chess.
 */
class ChessMovementRule : public MovementRule {

public:

   /**
    * Checks if this pieces move is valid based on the rules defined in this
    * class.
    *
    * @param piece The piece moving.
    * @param newRank The rank the piece is moving to.
    * @param newFile The file the piece is moving to.
    *
    * @return True if the king is safe.
    */
   bool isValidMove(Piece* piece, int newRank, int newFile) override;

   /**
    * Called after the piece has moved.
    */
   void afterMoveCall(Piece* piece) override;

   /**
    * @return The singleton for this class.
    */
   static ChessMovementRule* getSingleton();

   /**
    * Checks if a king is in check.
    *
    * @param king The king to check for.
    *
    * @return True if king is in check.
    */
   bool isKingInCheck(King* king);

private:

   /**
    * Private constructor for singleton.
    */
   ChessMovementRule();

   /**
    * Checks if the King on the same team of the piece is endangered by this
    * current move.
    *
    * @param piece The piece moving.
    * @param newRank The rank the piece is moving to.
    * @param newFile The file the piece is moving to.
    *
    * @return True if the king is safe.
    */
   bool checkIfMoveEndangersKing(Piece* piece, int newRank, int newFile);

   /**
    * Checks if a king is threatened by a knight.
    *
    * @param king King to check safety of.
    * @param rankToCheck Rank of tile to check for.
    * @param fileToCheck File of tile to check for.
    *
    * @return True if threatened by a knight.
    */
   bool isThreatenedByKnight(King* king, int rankToCheck, int fileToCheck);

   /**
    * Checks if this piece is a king, queen, or rook.
    *
    * @param king The king to check if safe.
    * @param foundPiece The piece to check.
    *
    * @return True if this piece is a king, queen, or rook.
    */
   bool isStraightMovingPiece(King* king, Piece* foundPiece);

   /**
    * Checks if this piece is a king, queen, bishop, or pawn (for correct
    * movement direction).
    *
    * @param king
    * @param foundPiece
    * @param pawnDirection
    *
    * @return If this piece is a king, queen, bishop, or pawn (for correct
    * movement direction).
    */
   bool isDiagonalMovingPiece(King* king, Piece* foundPiece,
      Pawn::MovementDirection pawnDirection);


   static ChessMovementRule* _singleton;
};


#endif //CHESSGAME_CHESSMOVEMENTRULE_H

#ifndef CHESSGAME_CHESSBOARD_H
#define CHESSGAME_CHESSBOARD_H

#include <string>
#include "GameBoard.h"
#include "Tile.h"
#include "Pawn.h"
#include "ChessGame.h"

class Tile;

/**
 * Represents a chess board.
 */
class ChessBoard : public GameBoard {

public:
   static const int DIMENSION = 8;

   /**
    * Constructs a new ChessBoard with pieces in correct starting locations.
    * @param game The game this board belongs to.
    */
   explicit ChessBoard(ChessGame* game);

   /**
    * Gets a tile on this board.
    *
    * @param rank Rank of the tile.
    * @param file File of the tile.
    *
    * @return The tile specified.
    */
   Tile* getTile(int rank, int file) override;

   /**
    * @return The length of the rank side of the board.
    */
   int getRankDimension();

   /**
    * @return The length of the file side of the board.
    */
   int getFileDimension();

   /**
    * @return A vector containing every piece on this board
    */
   std::vector<Piece*> getOccupyingPieces() override;

   /**
    * @return A string representation of this board.
    */
   std::string toString() override;

   /**
    * Returns the piece that starts on this tile at the beginning of a game
    * of chess.
    *
    * @param startingTile The tile to start on.
    *
    * @return The piece that starts on this tile. Nullptr if no piece starts
    * here at the start of game.
    */
   static Piece* getPieceForStartingLocation(Tile* startingTile);


private:

   /**
    * Adds a line of letters to a string, starting at 'a' and counting up in the
    * alphabet.
    *
    * @param appendTo String to append to.
    * @param numberToAdd The number of letters to add.
    */
   void addLineOfLetters(std::string& appendTo, int numberToAdd);

   /**
    * Adds a line of "+----+" strings to a string.
    *
    * @param appendTo The string to append to.
    * @param sectionsToAdd The number of "+----+" strings to add.
    */
   void addBorder(std::string& appendTo, int sectionsToAdd);


   Tile* _tiles[DIMENSION][DIMENSION];
   int _rankDimension;
   int _fileDimension;
};


#endif //CHESSGAME_CHESSBOARD_H

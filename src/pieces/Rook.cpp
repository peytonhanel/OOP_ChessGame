#include "Rook.h"

Rook::Rook(Team* team, Tile* startingTile)
   : Piece(team, startingTile, type())
{ }

Rook::~Rook() = default;

std::string Rook::getSymbol()
{
   return _team->getShade() == Team::Shade::light ? "wR" : "bR";
}

bool Rook::moveTo(int rank, int file)
{
   bool moved = false;
   int fileSteps = file - _occupyingTile->getFile();
   int rankSteps = rank - _occupyingTile->getRank();

   // move diagonal
   if ((fileSteps && !rankSteps) || (!fileSteps && rankSteps)) {
      moved = Piece::moveTo(rank, file, false, true);
   }
   return moved;
}



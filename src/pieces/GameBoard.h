#ifndef CHESSGAME_GAMEBOARD_H
#define CHESSGAME_GAMEBOARD_H

#include <string>
#include <vector>

class ChessGame;
class Tile;
class Piece;


/**
 * An abstract gameboard.
 */
class GameBoard {

public:

   /**
    * Constructs a new gameboard.
    *
    * @param game The game this board belongs to.
    */
   explicit GameBoard(ChessGame* game);

   /**
    * Returns a tile on this board.
    *
    * @param rank The rank of the tile to get.
    * @param file The file of the tile to get.
    *
    * @return A tile on this board.
    */
   virtual Tile* getTile(int rank, int file) = 0;

   /**
    * @return A vector with all the pieces on this board.
    */
   virtual std::vector<Piece*> getOccupyingPieces() = 0;

   /**
    * @return The game this board belongs to.
    */
   ChessGame* getGame();

    /**
    * @return String representation of the board.
    */
   virtual std::string toString() = 0;

protected:

   ChessGame* _game;
   std::vector<Piece*> _occupyingPieces;
};


#endif //CHESSGAME_GAMEBOARD_H

#include "ChessMovementRule.h"

ChessMovementRule* ChessMovementRule::_singleton;


ChessMovementRule::ChessMovementRule() = default;

bool ChessMovementRule::isKingInCheck(King* king)
{
   int kingRank = king->getOccupyingTile()->getRank();
   int kingFile = king->getOccupyingTile()->getFile();

   bool safe = true;
   Piece* foundPiece = nullptr;
   bool upBlocked = false;
   bool downBlocked = false;
   bool rightBlocked = false;
   bool leftBlocked = false;
   bool upRightBlocked = false;
   bool upLeftBlocked = false;
   bool downRightBlocked = false;
   bool downLeftBlocked = false;

   // checks all tiles that can reach the king by moving in a straight line away
   // from the king in every direction
   for (int i = 1; i < ChessBoard::DIMENSION && safe; i++)
   {
      // checks all tiles straight up from king, without walking off the board
      if (kingFile + i < ChessBoard::DIMENSION && !upBlocked)
      {
         // if any piece is occupied, check if it is a threat
         if (king->getOccupyingTile()->getBoard()
            ->getTile(kingRank, kingFile + i)->isOccupied())
         {
            upBlocked = true;
            foundPiece = king->getOccupyingTile()->getBoard()
               ->getTile(kingRank, kingFile + i)->getOccupant();

            // if foundPiece can attack us, king is NOT safe!
            if (isStraightMovingPiece(king, foundPiece)) {
               safe = false;
            }
         }
      }// if()

      // checks all tiles straight down from king
      if (kingFile - i >= 0 && !downBlocked && safe)
      {
         // if any piece is occupied, check if it is a threat
         if (king->getOccupyingTile()->getBoard()
            ->getTile(kingRank, kingFile - i)->isOccupied())
         {
            downBlocked = true;
            foundPiece = king->getOccupyingTile()->getBoard()
               ->getTile(kingRank, kingFile - i)->getOccupant();

            // if foundPiece can attack us, king is NOT safe!
            if (isStraightMovingPiece(king, foundPiece)) {
               safe = false;
            }
         }
      }// if()

      // checks all tiles straight right of king
      if (kingRank + i < ChessBoard::DIMENSION && !rightBlocked  && safe)
      {
         // if any piece is occupied, check if it is a threat
         if (king->getOccupyingTile()->getBoard()
            ->getTile(kingRank + i, kingFile)->isOccupied())
         {
            rightBlocked = true;
            foundPiece = king->getOccupyingTile()->getBoard()
               ->getTile(kingRank + i, kingFile)->getOccupant();

            // if foundPiece can attack us, king is NOT safe!
            if (isStraightMovingPiece(king, foundPiece)) {
               safe = false;
            }
         }
      }// if()

      // checks all tiles straight left of king
      if (kingRank - i >= 0 && !leftBlocked && safe)
      {
         // if any piece is occupied, check if it is a threat
         if (king->getOccupyingTile()->getBoard()
            ->getTile(kingRank - i, kingFile)->isOccupied())
         {
            leftBlocked = true;
            foundPiece = king->getOccupyingTile()->getBoard()
               ->getTile(kingRank - i, kingFile)->getOccupant();

            // if foundPiece can attack us, king is NOT safe!
            if (isStraightMovingPiece(king, foundPiece)) {
               safe = false;
            }
         }
      }// if()

      // checks all tiles diagonally up and right of the king
      if (  kingRank + i < ChessBoard::DIMENSION
         && kingFile + i < ChessBoard::DIMENSION
         && !upRightBlocked && safe)
      {
         // if any piece is occupied, check if it is a threat
         if (king->getOccupyingTile()->getBoard()
            ->getTile(kingRank + i, kingFile + i)->isOccupied())
         {
            upRightBlocked = true;
            foundPiece = king->getOccupyingTile()->getBoard()
               ->getTile(kingRank + i, kingFile + i)->getOccupant();

            // if foundPiece can attack us, king is NOT safe!
            if (isDiagonalMovingPiece(king, foundPiece, Pawn::Down)) {
               safe = false;
            }
         }
      }// if()

      // checks all tiles diagonally up and left of the king
      if (  kingRank - i >= 0
         && kingFile + i < ChessBoard::DIMENSION
         && !upLeftBlocked && safe)
      {
         // if any piece is occupied, check if it is a threat
         if (king->getOccupyingTile()->getBoard()
            ->getTile(kingRank - i, kingFile + i)->isOccupied())
         {
            upLeftBlocked = true;
            foundPiece = king->getOccupyingTile()->getBoard()
               ->getTile(kingRank - i, kingFile + i)->getOccupant();

            // if foundPiece can attack us, king is NOT safe!
            if (isDiagonalMovingPiece(king, foundPiece, Pawn::Down)) {
               safe = false;
            }
         }
      }// if()

      // checks all tiles diagonally down and right of the king
      if (  kingRank + i < ChessBoard::DIMENSION
         && kingFile - i >= 0
         && !downRightBlocked && safe)
      {
         // if any piece is occupied, check if it is a threat
         if (king->getOccupyingTile()->getBoard()
            ->getTile(kingRank + i, kingFile - i)->isOccupied())
         {
            downRightBlocked = true;
            foundPiece = king->getOccupyingTile()->getBoard()
               ->getTile(kingRank + i, kingFile - i)->getOccupant();

            // if foundPiece can attack us, king is NOT safe!
            if (isDiagonalMovingPiece(king, foundPiece, Pawn::Up)) {
               safe = false;
            }
         }
      }// if()

      // checks all tiles diagonally down and left of the king
      if (  kingRank - i >= 0
         && kingFile - i >= 0
         && !downLeftBlocked && safe)
      {
         // if any piece is occupied, check if it is a threat
         if (king->getOccupyingTile()->getBoard()
            ->getTile(kingRank - i, kingFile - i)->isOccupied())
         {
            downLeftBlocked = true;
            foundPiece = king->getOccupyingTile()->getBoard()
               ->getTile(kingRank - i, kingFile - i)->getOccupant();

            // if foundPiece can attack us, king is NOT safe!
            if (isDiagonalMovingPiece(king, foundPiece, Pawn::Up)) {
               safe = false;
            }
         }
      }// if()
   }// for()

   // the following 4 if statements checks for knights at vulnerable positions

   // checks for knights at two positions above king.
   if (safe && kingFile + 2 < ChessBoard::DIMENSION)
   {
      if ((kingRank + 1 < ChessBoard::DIMENSION
           && isThreatenedByKnight(king, kingRank + 1, kingFile + 2))
          || (kingRank - 1 >= 0
              && isThreatenedByKnight(king, kingRank - 1, kingFile + 2)))
      {
         safe = false;
      }
   }

   // checks for knights at two positions below king.
   if (safe && kingFile - 2 >= 0)
   {
      if ((kingRank + 1 < ChessBoard::DIMENSION
          && isThreatenedByKnight(king, kingRank + 1, kingFile - 2))
          || (kingRank - 1 >= 0
          && isThreatenedByKnight(king, kingRank - 1, kingFile - 2)))
      {
         safe = false;
      }
   }

   // checks for knights at two positions right of king
   if (safe && kingRank + 2 < ChessBoard::DIMENSION)
   {
      if ((kingFile + 1 < ChessBoard::DIMENSION
          && isThreatenedByKnight(king, kingRank + 2, kingFile + 1))
          || (kingFile - 1 >= 0
          && isThreatenedByKnight(king, kingRank + 2, kingFile - 1)))
      {
         safe = false;
      }
   }

   // checks for knights at two positions left of king
   if (safe && kingRank - 2 >= 0)
   {
      if ((kingFile + 1 < ChessBoard::DIMENSION
          && isThreatenedByKnight(king, kingRank - 2, kingFile + 1))
          || (kingFile - 1 >= 0
          && isThreatenedByKnight(king, kingRank - 2, kingFile - 1)))
      {
         safe = false;
      }
   }
   return !safe;
}

bool ChessMovementRule::checkIfMoveEndangersKing(Piece* piece,
   int newRank, int newFile)
{
   bool inCheck;
   King* king = piece->getOccupyingTile()->getBoard()->getGame()->
      getKingForTeam(piece->getTeam());

   // simulates a move for piece to newRank and newFile
   Tile* pieceOriginalTile = piece->getOccupyingTile();
   Piece* destOccupant = piece->getOccupyingTile()->getBoard()
      ->getTile(newRank, newFile)->getOccupant();

   piece->movePiece(newRank, newFile);

   // is king in check?
   inCheck = isKingInCheck(king);

   if (!inCheck) {
      king->setCheckState(inCheck);
   }

   // ends simulation of moved piece, moves pieces back to original locations
   piece->movePiece(pieceOriginalTile->getRank(), pieceOriginalTile->getFile());
   if (destOccupant != nullptr) {
      destOccupant->getOccupyingTile()->setOccupant(destOccupant);
   }
   return inCheck;
}

bool ChessMovementRule::isThreatenedByKnight(King* king, int rankToCheck,
   int fileToCheck)
{
   bool threatened = false;
   Piece* foundPiece;

   // if tile is occupied check if occupant is a threatening knight
   if (king->getOccupyingTile()->getBoard()
      ->getTile(rankToCheck, fileToCheck)->isOccupied())
   {
      foundPiece = king->getOccupyingTile()->getBoard()
         ->getTile(rankToCheck, fileToCheck)->getOccupant();

      // not safe if occupant foundPiece is a threatening knight
      if (foundPiece->getTeam() != king->getTeam()
          && foundPiece->getType() == Knight::type())
      {
         threatened = true;
      }
   }
   return threatened;
}

bool ChessMovementRule::isStraightMovingPiece(King* king, Piece* foundPiece)
{
   bool canAttack = false;

   // is foundPiece on enemy team, and is it a piece that can attack the king?
   if (foundPiece->getTeam() != king->getTeam())
   {
      // if found piece is of a type that can attack non-diagonally
      if (  foundPiece->getType() == King::type()
         || foundPiece->getType() == Queen::type()
         || foundPiece->getType() == Rook::type())
      {
         // if foundPiece is an enemy king
         if (foundPiece->getType() == King::type()) {

            // if that king is one tile away
            if (abs(foundPiece->getOccupyingTile()->getFile()
                    - king->getOccupyingTile()->getFile()) == 1
                || abs(foundPiece->getOccupyingTile()->getRank()
                       - king->getOccupyingTile()->getRank()) == 1)
            {
               canAttack = true;
            }
         }
         else {
            canAttack = true;
         }
      }
   }
   return canAttack;
}

bool ChessMovementRule::isDiagonalMovingPiece(King* king, Piece* foundPiece,
   Pawn::MovementDirection pawnDirection)
{
   bool canAttack = false;

   // if found piece and king are on different teams
   if (foundPiece->getTeam() != king->getTeam()) {

      // if found piece is of a type that attacks diagonally
      if (   foundPiece->getType() == King::type()
         ||  foundPiece->getType() == Queen::type()
         ||  foundPiece->getType() == Bishop::type()
         || (foundPiece->getType() == Pawn::type()))
      {
         // if found piece is a pawn
         if (foundPiece->getType() == Pawn::type())
         {
            // if pawn is close enough to attack
            if (((Pawn*)foundPiece)->getDirection() == pawnDirection
                && abs(foundPiece->getOccupyingTile()->getRank() - king->getOccupyingTile()->getRank()) == 1
                && abs(foundPiece->getOccupyingTile()->getFile() - king->getOccupyingTile()->getFile()) == 1)
            {
               canAttack = true;
            }
         }
         // if foundPiece is an enemy king
         else if (foundPiece->getType() == King::type()) {

            // if that king is one tile away
            if (abs(foundPiece->getOccupyingTile()->getFile()
                    - king->getOccupyingTile()->getFile()) == 1
                || abs(foundPiece->getOccupyingTile()->getRank()
                       - king->getOccupyingTile()->getRank()) == 1)
            {
               canAttack = true;
            }
         }
            // piece is a queen, bishop, or king
         else {
            canAttack = true;
         }
      }
   }
   return canAttack;
}

bool ChessMovementRule::isValidMove(Piece* piece, int newRank, int newFile)
{
   return !checkIfMoveEndangersKing(piece, newRank, newFile);
}

void ChessMovementRule::afterMoveCall(Piece* piece)
{
   Team* otherTeam;
   King* otherKing;
   Queen* promotionQueen;

   // gets the opposite team
   if (piece->getTeam() == Team::getLightSingleton()) {
      otherTeam = Team::getDarkSingleton();
   }
   else {
      otherTeam = Team::getLightSingleton();
   }
   // checks if other king is in check;
   otherKing = piece->getOccupyingTile()->getBoard()->getGame()->
      getKingForTeam(otherTeam);
   otherKing->setCheckState(isKingInCheck(otherKing));

   // promotes pawns if they reach the other side of the board
   if (piece->getType() == Pawn::type()) {

      // deletes the pawn and replaces it with a queen
      if ((dynamic_cast<Pawn*>(piece)->getDirection() == Pawn::Up
         && piece->getOccupyingTile()->getFile() == ChessBoard::DIMENSION - 1)
         || (dynamic_cast<Pawn*>(piece)->getDirection() == Pawn::Down
            && piece->getOccupyingTile()->getFile() == 0))
      {
         promotionQueen = new Queen(piece->getTeam(),
                                    piece->getOccupyingTile());
         piece->getOccupyingTile()->setOccupant(promotionQueen);
         promotionQueen->getOccupyingTile()->setOccupant(promotionQueen);
         delete dynamic_cast<Pawn*>(piece);
      }
   }
}

ChessMovementRule* ChessMovementRule::getSingleton()
{
   // creates singleton if not already created
   if (_singleton == nullptr) {
      _singleton = new ChessMovementRule();
   }
   return _singleton;
}

#ifndef CHESSGAME_MOVEMENTRULE_H
#define CHESSGAME_MOVEMENTRULE_H

#include "Piece.h"



class Piece;

/**
 * Class that contains extra rules for pieces in a generic board game.
 */
class MovementRule {

public:

   /**
    * Checks if the move of a certain piece is valid, aside from it's normal
    * movement rules.
    *
    * @param piece The piece moving.
    * @param newRank The rank of the tile the piece is moving to.
    * @param newFile The file of the tile the piece is moving to.
    *
    * @return True if this move is allowed.
    */
   virtual bool isValidMove(Piece* piece, int newRank, int newFile) = 0;

   /**
    * Performs any necessary checks after the piece is moved.
    */
   virtual void afterMoveCall(Piece*) = 0;
};


#endif //CHESSGAME_MOVEMENTRULE_H

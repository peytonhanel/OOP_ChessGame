#ifndef CHESSGAME_PIECE_H
#define CHESSGAME_PIECE_H

#include <string>
#include "MovementRule.h"
#include "Tile.h"
#include "Team.h"

/**
 * Represents an abstract piece for a checkered game board. Contains movement
 * behavior. Concrete child classes can override moveTo() with a method that
 * contains specific movement rules, and then calls upon Piece::moveTo() to
 * execute the movement.
 */
class Piece {

public:

   /**
    * Moves this piece to another tile on the board along the shortest path.
    * Performs an attack if that tile is occupied by a piece of a different
    * team. Move only valid if the shortest path to the tile is unobstructed.
    *
    * @param rank The rank of the tile to move to.
    * @param file The file of the tile to move to.
    *
    * @return True if the piece moved successfully.
    */
   virtual bool moveTo(int rank, int file) = 0;

   /**
    * Moves this piece regardles if it is a valid move or not.
    *
    * @param newRank The rank to move to.
    * @param newFile The file to move to.
    */
   void movePiece(int newRank, int newFile);

   /**
    * @return The team this Piece belongs to.
    */
   Team* getTeam();

   /**
    * @return True if this piece has moved at least once.
    */
   bool hasNotMoved();

   /**
    * @return The tile this piece was on when the game began.
    */
   Tile* getStartingTile();

   /**
    * @return The tile that this piece is currently occupying.
    */
   Tile* getOccupyingTile();

   /**
    * @return The name of this class.
    */
   const std::string& getType();

   /**
    * @return The textual symbol that represents this piece, for console games.
    */
   virtual std::string getSymbol() = 0;

protected:

   /**
   * Constructor that is called by concrete child classes.
   *
   * @param team The team this piece belongs to.
   * @param startingTile The tile that this piece starts the game on.
   */
   Piece(Team* team, Tile* startingTile, const std::string& type);

   /**
    * Destructor for a piece.
    */
   virtual ~Piece();

   /**
    * Moves a piece to another tile on the board along the shortest path.
    * Performs an attack if that tile is occupied by a piece of a different
    * team. If jump is set to false, the piece will not move if the shortest
    * path to the specified tile is obstructed. If set to true, the piece will
    * "jump" there by moving directly to it regardless if there is or is not a
    * clear path.
    *
    * @param rank The rank of the tile to move to.
    * @param file The file of the tile to move to.
    * @param jump If true, this piece will move directly to the specified tile.
    *             If false, the piece can only move if the shortest path to the
    *             tile is not obstructed by any piece.
    *
    * @return True if the piece moved successfully.
    */
   bool moveTo(int rank, int file, bool jump, bool allowAttack);

   /**
    * Checks if a there is a clear path starting from where this piece is to
    * where it is going. Meaning this return true if all the tiles between the
    * start and the destination are unocuppied in the shortest path. Does not
    * check if the destination tile is occupied.
    *
    * @param moveToRank The rank of the destination tile.
    * @param moveToFile The file of the destination tile.
    *
    * @return True if the shortest path is clear between the tile this piece
    * currently is and the destination tile (exclusive).
    */
   bool shortestPathIsClear(int moveToRank, int moveToFile);

   Tile* _startingTile;
   Tile* _occupyingTile;
   Team* _team;
   bool _hasNotMoved;
   std::string _type;
};


#endif //CHESSGAME_PIECE_H

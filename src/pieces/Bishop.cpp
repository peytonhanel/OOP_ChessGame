#include "Bishop.h"


Bishop::Bishop(Team* team, Tile* startingTile)
   : Piece(team, startingTile, type())
{ }

Bishop::~Bishop() = default;

std::string Bishop::getSymbol()
{
   return _team->getShade() == Team::Shade::light ? "wB" : "bB";
}

bool Bishop::moveTo(int rank, int file)
{
   bool moved = false;
   int fileSteps = file - _occupyingTile->getFile();
   int rankSteps = rank - _occupyingTile->getRank();

   // move diagonal
   if (abs(fileSteps) == abs(rankSteps)) {
      moved = Piece::moveTo(rank, file, false, true);
   }
   return moved;
}

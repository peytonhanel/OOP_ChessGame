#include "Tile.h"
#include "Piece.h"


Tile::Tile(int rank, int file, GameBoard* board)
{
   _rank = rank;
   _file = file;
   _board = board;
}

bool Tile::isOccupied()
{
   return _occupant != nullptr;
}

Piece* Tile::getOccupant()
{
   return _occupant;
}

void Tile::setOccupant(Piece* piece)
{
   _occupant = piece;
}

int Tile::getRank()
{
   return _rank;
}

int Tile::getFile()
{
   return _file;
}

GameBoard* Tile::getBoard()
{
   return _board;
}

std::string Tile::toString()
{
   // Gets the symbol of the occupying piece (if there is one)
   std::string s;
   s += (_occupant != nullptr) ? _occupant->getSymbol() : "  ";
   return s;
}

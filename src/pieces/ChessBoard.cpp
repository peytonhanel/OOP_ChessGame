#include <vector>
#include "ChessBoard.h"


ChessBoard::ChessBoard(ChessGame* game) : GameBoard(game)
{
   _rankDimension = DIMENSION;
   _fileDimension = DIMENSION;

   // Initializes a new chess board, placing pieces in their starting positions.
   for (int rank = 0; rank < _rankDimension; rank++) {
      for (int file = 0; file < _fileDimension; file++)
      {
         _tiles[rank][file] = new Tile(rank, file, this);
         _tiles[rank][file]
            ->setOccupant(getPieceForStartingLocation(_tiles[rank][file]));

         // adds the generated piece to the list of pieces on this board
         if (_tiles[rank][file]->isOccupied()) {
            _occupyingPieces.push_back(_tiles[rank][file]->getOccupant());

            // adds kings to list of kings in the game
            if (_tiles[rank][file]->getOccupant()->getType() == King::type()) {
               game->listKing(dynamic_cast<King*>
                  (_tiles[rank][file]->getOccupant()));
            }
         }
      }
   }
}

Tile* ChessBoard::getTile(int rank, int file)
{
   return _tiles[rank][file];
}

int ChessBoard::getRankDimension()
{
   return _rankDimension;
}

int ChessBoard::getFileDimension()
{
   return _fileDimension;
}

std::vector<Piece*> ChessBoard::getOccupyingPieces()
{
   return _occupyingPieces;
}

std::string ChessBoard::toString()
{
   std::string boardString;
   addLineOfLetters(boardString, getFileDimension());
   addBorder(boardString, getFileDimension());

   // calls Tile::toString for each file of tiles
   for (int file = 0; file < getFileDimension(); file++)
   {
      // prints the numerical ranks at the left of the board
      boardString.append(std::to_string(getFileDimension() - file) + " | ");

      // Calls Tile::toString for each rank in this file.
      for (int rank = 0; rank < getRankDimension(); rank++) {
         boardString.append
            (getTile(rank, getFileDimension() - 1 - file)->toString());
         boardString.append(" | ");
      }
      // prints the numerical ranks at the right of the board
      boardString.append(std::to_string(getFileDimension() - file) + "\n");
      addBorder(boardString, getFileDimension());
   }
   addLineOfLetters(boardString, getFileDimension());
   boardString.append("\n");

   // announces if the kings are in check
   if (_game->getKingForTeam(Team::getLightSingleton())->isInCheck()) {
      boardString.append("White King is in Check!\n");
   }
   if (_game->getKingForTeam(Team::getDarkSingleton())->isInCheck()) {
      boardString.append("Black King is in Check!\n");
   }
   return boardString;
}

void ChessBoard::addLineOfLetters(std::string& appendTo, int numberToAdd)
{
   char currentLetter = 'a';
   appendTo.append("     ");

   // prints letters starting at 'a' and incrementing up the alphabet
   for (int i = 0; i < numberToAdd; i++) {
      appendTo += +currentLetter;
      appendTo += "    ";
      currentLetter++;
   }
   appendTo.append("\n");
}

void ChessBoard::addBorder(std::string& appendTo, int sectionsToAdd)
{
   // prints "+----+" * sections to add.
   appendTo.append("  ");
   for (int i = 0; i < sectionsToAdd; ++i) {
      appendTo.append("+----");
   }
   appendTo.append("+\n");
}

Piece* ChessBoard::getPieceForStartingLocation(Tile* startingTile)
{
   const int ROOK_START_1 = 0;
   const int ROOK_START_2 = 7;
   const int KNIGHT_START_1 = 1;
   const int KNIGHT_START_2 = 6;
   const int BISHOP_START_1 = 2;
   const int BISHOP_START_2 = 5;
   const int QUEEN_START = 3;
   const int KING_START = 4;

   Piece* piece = nullptr;

   // generates pawns for correct tiles (black team)
   if (startingTile->getFile() == ChessBoard::DIMENSION - 2) {
      piece = new Pawn(Team::getDarkSingleton(), startingTile,
                       Pawn::MovementDirection::Down);
      dynamic_cast<Pawn*>(piece)->giveSecondStep();
   }
   // generates pawns for correct tiles (white team)
   else if (startingTile->getFile() == 1) {
      piece = new Pawn(Team::getLightSingleton(), startingTile,
                       Pawn::MovementDirection::Up);
      dynamic_cast<Pawn*>(piece)->giveSecondStep();
   }
   // generates non-pawn pieces for correct tiles
   else {
      switch (startingTile->getRank())
      {
         // creates a new Rook
         case ROOK_START_1:
         case ROOK_START_2:
            // dark team
            if (startingTile->getFile() == ChessBoard::DIMENSION - 1) {
               piece = new Rook(Team::getDarkSingleton(), startingTile);
            }
            // light team
            else if (startingTile->getFile() == 0) {
               piece = new Rook(Team::getLightSingleton(), startingTile);
            }
            break;

         // creates a new Knight
         case KNIGHT_START_1:
         case KNIGHT_START_2:
            // dark team
            if (startingTile->getFile() == ChessBoard::DIMENSION - 1) {
               piece = new Knight(Team::getDarkSingleton(), startingTile);
            }
            // light team
            else if (startingTile->getFile() == 0) {
               piece = new Knight(Team::getLightSingleton(), startingTile);
            }
            break;

         // creates a new Bishop
         case BISHOP_START_1:
         case BISHOP_START_2:
            // dark team
            if (startingTile->getFile() == ChessBoard::DIMENSION - 1) {
               piece = new Bishop(Team::getDarkSingleton(), startingTile);
            }
            // light team
            else if (startingTile->getFile() == 0) {
               piece = new Bishop(Team::getLightSingleton(), startingTile);
            }
            break;

         // creates a new Queen
         case QUEEN_START:
            //dark team
            if (startingTile->getFile() == ChessBoard::DIMENSION - 1) {
               piece = new Queen(Team::getDarkSingleton(), startingTile);
            }
            // light team
            else if (startingTile->getFile() == 0) {
               piece = new Queen(Team::getLightSingleton(), startingTile);
            }
            break;

         // creates a new King
         case KING_START:
            // dark team
            if (startingTile->getFile() == ChessBoard::DIMENSION - 1) {
               piece = new King(Team::getDarkSingleton(), startingTile);
            }
            // light team
            else if (startingTile->getFile() == 0) {
               piece = new King(Team::getLightSingleton(), startingTile);
            }
            break;
         default:
            break;
      }
   }
   return piece;
}



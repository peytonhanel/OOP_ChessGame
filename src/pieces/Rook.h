#ifndef CHESSGAME_ROOK_H
#define CHESSGAME_ROOK_H

#include "Piece.h"
#include "Team.h"


/**
 * Class that represents a Rook.
 */
class Rook : public Piece {

public:
   /**
    * Constructs a rook.
    *
    * @param team The team this rook belongs to.
    * @param startingTile The tile this rook starts the game on.
    */
   Rook(Team* team, Tile* startingTile);

   /**
    * Destructs this Rook.
    */
   ~Rook() override;

   /**
   * Moves this Rook to a new tile.
   *
   * @param rank The rank of the tile being moved to.
   * @param file The file of the tile being moved to.
   *
   * @return True if moved successfully.
   */
   bool moveTo(int rank, int file) override;

   /**
    * @return The symbol representing this rook.
    */
   std::string getSymbol() override;

   /**
    * @return The name of this class.
    */
   static const std::string& type()
   {
      static std::string type("Rook");
      return type;
   }
};


#endif //CHESSGAME_ROOK_H

#include "Piece.h"
#include "ChessGame.h"


Piece::Piece(Team* team, Tile* startingTile, const std::string& type)
{
   _startingTile = startingTile;
   _occupyingTile = startingTile;
   _team = team;
   _hasNotMoved = true;
   _type = type;
}

Piece::~Piece() = default;

bool Piece::moveTo(int rank, int file, bool jump, bool allowAttack)
{
   bool moved = false;
   _hasNotMoved = false;
   Piece* pieceAtDestination
      = _occupyingTile->getBoard()->getTile(rank, file)->getOccupant();

   // moves piece
   if (((!jump && shortestPathIsClear(rank, file)) || jump)
       && getOccupyingTile()->getBoard()->getGame()->
          isValidMove(this, rank, file))
   {
      // move the piece to destination
      if (pieceAtDestination == nullptr) {
         moved = true;
         movePiece(rank, file);
      }
      // moves piece to destination and attacks the occupant there
      else if (pieceAtDestination->getTeam() != _team && allowAttack) {
         moved = true;
         delete _occupyingTile->getBoard()->getTile(rank, file)->getOccupant();
         movePiece(rank, file);
      }
   }
   return moved;
}

void Piece::movePiece(int rank, int file)
{
   // moves the piece if the destination tile is unoccupied
   _occupyingTile->getBoard()->getTile(rank, file)->setOccupant(this);
   _occupyingTile->setOccupant(nullptr);
   _occupyingTile = _occupyingTile->getBoard()->getTile(rank, file);

}

bool Piece::shortestPathIsClear(int moveToRank, int moveToFile)
{
   bool clearPath = true;
   int rankSteps = moveToRank - _occupyingTile->getRank();
   int fileSteps = moveToFile - _occupyingTile->getFile();
   int rankDirection = 0;
   int fileDirection = 0;

   // if the piece is moving more than one tile away, check if all tiles between
   // the starting tile and destination tile are unoccupied
   if (   (abs(fileSteps)  > 1 && abs(rankSteps) >= 0)
       || (abs(fileSteps) >= 0 && abs(rankSteps)  > 1))
   {
      // calculates the direction the piece is moving
      if (rankSteps > 0) {
         rankDirection = 1;
      }
      else if (rankSteps < 0) {
         rankDirection = -1;
      }
      if (fileSteps > 0) {
         fileDirection = 1;
      }
      else if (fileSteps < 0) {
         fileDirection = -1;
      }
      // piece is moving straight forward or backwards
      if (fileSteps != 0 && rankSteps == 0)
      {
         // checks if this path is clear
         for (int i = _occupyingTile->getFile() + fileDirection;
              i != moveToFile && clearPath; i += fileDirection)
         {
            clearPath = !_occupyingTile->getBoard()->
               getTile(moveToRank, i)->isOccupied();
         }
      }
      // piece is moving straight right or left
      else if (fileSteps == 0 && rankSteps != 0)
      {
         // checks if this path is clear
         for (int i = _occupyingTile->getRank() + rankDirection;
              i != moveToRank && clearPath; i += rankDirection)
         {
            clearPath = !_occupyingTile->getBoard()->
               getTile(i, moveToFile)->isOccupied();
         }
      }
      // piece is moving diagonal
      else if (abs(fileSteps) == abs(rankSteps))
      {
         // checks if this path is clear
         for (int i = 1; i < abs(rankSteps) && clearPath; i++)
         {
            clearPath = !_occupyingTile->getBoard()
               ->getTile(_occupyingTile->getRank() + i * rankDirection,
                         _occupyingTile->getFile() + i * fileDirection)
               ->isOccupied();
         }
      }
      // Optional: add another else if statement for modding purposes.
      // this will check for pieces with the non-traditional movement of being
      // able to move anywhere on the board, meaning that the check will require
      // one straight movement and one diagonal movement. This will mean that
      // there will be two shortest paths of equal distance to the destination
      // that need to be checked.
   }
   return clearPath;
}

Team* Piece::getTeam()
{
   return _team;
}

bool Piece::hasNotMoved()
{
   return _hasNotMoved;
}

Tile* Piece::getStartingTile()
{
   return _startingTile;
}

Tile* Piece::getOccupyingTile()
{
   return _occupyingTile;
}

const std::string& Piece::getType()
{
   return _type;
}

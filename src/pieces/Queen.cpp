#include "Queen.h"

Queen::Queen(Team* team, Tile* tile)
   : Piece (team, tile, type())
{ }

Queen::~Queen() = default;

std::string Queen::getSymbol()
{
   return _team->getShade() == Team::Shade::light ? "wQ":  "bQ";
}

bool Queen::moveTo(int rank, int file)
{
   bool moved = false;
   int fileSteps = file - _occupyingTile->getFile();
   int rankSteps = rank - _occupyingTile->getRank();

   // move diagonal or move straight
   if ((abs(fileSteps) == abs(rankSteps))
       || ((fileSteps && !rankSteps) || (!fileSteps && rankSteps)))
   {
      moved = Piece::moveTo(rank, file, false, true);
   }
   return moved;
}



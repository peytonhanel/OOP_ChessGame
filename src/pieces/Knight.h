#ifndef CHESSGAME_KNIGHT_H
#define CHESSGAME_KNIGHT_H

#include "Team.h"
#include "Tile.h"
#include "Piece.h"


/**
 * Represents a Knight piece for a game of chess.
 */
class Knight : public Piece {

public:

   /**
    * Constructs a Knight chess piece.
    *
    * @param team The team this piece belongs to.
    * @param startingTile The
    */
   Knight(Team* team, Tile* startingTile);

   /**
    * Destructs this Knight.
    */
   ~Knight() override;

   /**
   * Moves this Knight to a new tile.
   *
   * @param rank The rank of the tile being moved to.
   * @param file The file of the tile being moved to.
   *
   * @return True if moved successfully.
   */
   bool moveTo(int rank, int file) override;

   /**
    * @return The textual symbol that represents this piece, for console games.
    */
   std::string getSymbol() override;

   /**
    * @return The name of this class.
    */
   static const std::string& type()
   {
      static std::string type("Knight");
      return type;
   }
};


#endif //CHESSGAME_KNIGHT_H

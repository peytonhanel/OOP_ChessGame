#include "Pawn.h"

Pawn::Pawn(Team* team, Tile* startingTile,
           Pawn::MovementDirection movementDirection)
   : Piece(team, startingTile, type())
{
   _movementDirection = movementDirection;
}

Pawn::~Pawn() = default;

void Pawn::giveSecondStep()
{
   _canMoveForwardTwo = true;
}

std::string Pawn::getSymbol()
{
   return _team->getShade() == Team::Shade::light ? "wP" : "bP";
}

bool Pawn::moveTo(int rank, int file)
{
   bool moved = false;
   int fileSteps = file - _occupyingTile->getFile();
   int rankSteps = rank - _occupyingTile->getRank();

   // if pawn is moving in correct direction
   if (  (_movementDirection == MovementDirection::Up   && fileSteps > 0)
      || (_movementDirection == MovementDirection::Down && fileSteps < 0))
   {
      // move forward one or two steps
      if (  (abs(fileSteps) == 2 && (!rankSteps && _canMoveForwardTwo))
         || (abs(fileSteps) == 1 && rankSteps == 0))
      {
         moved = Piece::moveTo(rank, file, false, false);
      }
      // attack diagonal
      else if (abs(fileSteps) == 1 && abs(rankSteps) == 1
         && _occupyingTile->getBoard()->getTile(rank, file)->isOccupied())
      {
         moved = Piece::moveTo(rank, file, false, true);
      }
   }
   // lose the second step on any kind of movement
   if (moved) {
      _canMoveForwardTwo = false;
   }
   return moved;
}

Pawn::MovementDirection Pawn::getDirection()
{
   return _movementDirection;
}

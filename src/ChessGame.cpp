#include "ChessGame.h"

MovementRule* ChessGame::_movementRule;
Team* ChessGame::_players[2];

using std::cin;
using std::cout;
using std::endl;


ChessGame::ChessGame(bool useGraphics, bool useAI)
{
   // gets singleton values
   if (_movementRule == nullptr) {
      _movementRule = ChessMovementRule::getSingleton();
   }
   if (_players[0] == nullptr) {
      _players[0] = Team::getLightSingleton();
   }
   if (_players[1] == nullptr) {
      _players[1] = Team::getDarkSingleton();
   }
   _usesGraphics = useGraphics;
   _usesAI = useAI;
   _board = new ChessBoard(this);
   _currentPlayerIndex = 0;
   _winner = nullptr;
}

bool ChessGame::hasWinner()
{
   return _winner != nullptr;
}

bool ChessGame::nextTurn(bool allowSurrender)
{
   Piece* pieceToMove;
   std::string rawInput;
   int pieceRank;
   int pieceFile;
   int moveToRank;
   int moveToFile;
   bool hasBadInput;

   // Gets input and error traps. Moves pieces if input is good.
   do {
      hasBadInput = false;

      // prompts for and retrieves movement input for current player.
      if (getCurrentPlayer()->getShade() == Team::Shade::dark) {
         cout << "Black Turn >> " << std::flush;
      }
      else {
         cout << "White Turn >> " << std::flush;
      }
      getline(cin, rawInput);

      // if the player inputs a command to end the game
      if (rawInput.length() == 1)
      {
         // if a player is in check, he can declare checkmate
         if (rawInput[0] == '#') {
            if (getKingForTeam(getCurrentPlayer())->isInCheck()) {
               _winner = getCurrentPlayer() == Team::getLightSingleton()
                       ? Team::getLightSingleton() : Team::getDarkSingleton();
            }
            else {
               cout << "King not in check!" << endl;
               hasBadInput = true;
            }
         }
         // player accepts surrender
         else if (allowSurrender && rawInput[0] == '=') {
            _winner = getCurrentPlayer();
         }
         else {
            cout << errorMsg() << endl;
            hasBadInput = true;
         }
      }
      // parses the input to get individual rank and file coordinates
      else if (rawInput.length() == INPUT_LENGTH
            || rawInput.length() == DRAW_INPUT_LENGTH)
      {
         allowSurrender = false;

         pieceRank = rawInput[START_RANK] - ASCII_a;
         pieceFile = rawInput[START_FILE] - ASCII_1;
         moveToRank = rawInput[DEST_RANK] - ASCII_a; //TODO: USE CONSTANTS
         moveToFile = rawInput[DEST_FILE] - ASCII_1;

         if (rawInput.length() == DRAW_INPUT_LENGTH
             && rawInput[DRAW_SPOT] == '=')
         {
            allowSurrender = true;
         }

         // Error Trapping. TODO: some input give segmentation faults (i.e. toeha)
         if (pieceRank < 0 || pieceRank >= _board->getRankDimension()
             || pieceFile < 0 || pieceFile >= _board->getRankDimension()
             || moveToRank < 0 || moveToRank >= _board->getRankDimension()
             || moveToFile < 0 || moveToFile >= _board->getFileDimension()
             || rawInput[SPACE_SPOT] != SPACE_CHAR)
         {
            std::cerr << errorMsg() << endl;
            hasBadInput = true;
         }

         // checks if tile specified by input is occupied, and if so, moves the
         // piece if it is of the correct team
         if (_board->getTile(pieceRank, pieceFile)->isOccupied()) {
            pieceToMove = _board->getTile(pieceRank, pieceFile)->getOccupant();

            // tells this game if there is a winner
            if (pieceToMove->getTeam() == getCurrentPlayer()
                && pieceToMove->moveTo(moveToRank, moveToFile))
            {
               _movementRule->afterMoveCall(pieceToMove);
            }
            else {
               std::cerr << errorMsg() << endl;
               hasBadInput = true;
            }
         }
         else {
            std::cerr << "ERROR: tile " << rawInput[START_RANK]
                      << rawInput[START_FILE] << " unoccupied." << endl;
            hasBadInput = true;
         }
      }
      else {
         hasBadInput = true;
      }
   }// do()
   while (hasBadInput);

   // sets current player to be the next player
   if (_currentPlayerIndex == 0) {
      _currentPlayerIndex = 1;
   }
   else {
      _currentPlayerIndex = 0;
   }
   return allowSurrender;
}

Team* ChessGame::getCurrentPlayer()
{
   return _players[_currentPlayerIndex];
}

void ChessGame::initialize()
{
   bool allowSurrender = false;

   // displays the state of the board and requests input for the next turn
   while (!hasWinner()) {
      cout << _board->toString() << endl;
      allowSurrender = nextTurn(allowSurrender);
   }
   cout << _board->toString() << endl;
   cout << "GAME OVER!!" << endl;
}

bool ChessGame::listKing(King* king)
{
   bool excludesKing = false;

   // checks if this king is already listed
   for (King* listedKing : _kings) {
      excludesKing = king == listedKing;
   }
   // lists king
   if (!excludesKing) {
      _kings.push_back(king);
   }
   return excludesKing;
}

bool ChessGame::isValidMove(Piece* piece, int newRank, int newFile)
{
   return _movementRule->isValidMove(piece, newRank, newFile);
}

King* ChessGame::getKingForTeam(Team* team)
{
   // searches for the king that is of team and returns it.
   King* foundKing = nullptr;
   bool found = false;
   for (unsigned int i = 0; i < _kings.size() && !found; i++) {
      if (_kings[i]->getTeam() == team) {
         found = true;
         foundKing = _kings[i];
      }
   }
   return foundKing;
}

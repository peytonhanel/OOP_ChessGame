This project is a command line chess program written in C++ for my class CSIS 370 Object Oriented Design and Analysis. The program took several weeks to write for the purpose of learning and demonstrating object oriented design principals and software engineering. The project began with a design in UML and a sequence diagram. GitLab was used for version control. A lot of time was spent learning the nuances of C++ so that the compiler would be happy and run, and in the end a well rounded and fundamental understanding of low-level programming was achieved.

If you want to run it yourself, download the repo and either compile in in command line or open the project in an IDE.

# Developer Info

Created by Peyton Hanel
